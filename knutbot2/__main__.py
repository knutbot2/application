import time
import logging
import argparse
import json
import asyncio

from knutbot2 import (
    config,
    evaluator,
    translator,
    database,
    __version__,
    )
from knutbot2.twitch.bot import TwitchBot
from knutbot2.discord.bot import DiscordBot


FORMAT = "[%(asctime)s %(levelname)8s] %(message)s"


def main() -> None:
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', help="Config file", default='config.json')
    args = parser.parse_args()
    with open(args.config) as f:
        config.config.update(json.load(f))
    logging.basicConfig(format=FORMAT, level=logging.INFO)
    logging.info(f"Version: {__version__}")
    loop = asyncio.get_event_loop()
    database.init()
    evaluator.init()
    translator.init()
    if config.config['twitch']['active']:
        TwitchBot.init()
    if config.config['discord']['active']:
        DiscordBot.init()

    loop.run_forever()


main()
