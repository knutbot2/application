from __future__ import annotations
import queue
import enum
import functools
import asyncio
import datetime
from typing import (
    Callable,
    List,
    Union,
    Any,
    Optional,
    TYPE_CHECKING
    )

from knutbot2.permission import (
    UserPermission,
    get_user_permission,
    get_cmd_required_permission,
    )
if TYPE_CHECKING:
    from knutbot2.database import (
        ChatGroup,
        User,
        )


class CommandParameter(object):
    """Class that will hold the parameter for a command execution"""
    def __init__(self,
                 chatgroup: ChatGroup,
                 user: User,
                 origin: CommandOrigin,
                 args: List[str]):
        # put something useful in here, right now just any args for testing
        self.chatgroup = chatgroup
        self.user = user
        self.args = args
        self.origin = origin


CommonCommandResultType = Callable[[queue.Queue, int, CommandParameter], None]


class CommonCommandWork(object):
    """Class that will be passed into the Queue of the evaluator with all
    the neccessary information to execute the command/function"""
    def __init__(self, fn: Callable,
                 answer_queue: queue.Queue,
                 trans_cache_id: int,
                 params: CommandParameter
                 ):
        self.fn = fn
        self.answer_queue = answer_queue
        self.params = params
        self.trans_cache_id = trans_cache_id

    def __str__(self) -> str:
        return f"WORK[{self.fn.__name__}]"


class AbstractCommand(object):

    required_permission: UserPermission

    def is_on_cooldown(self, user: User, chatgroup: ChatGroup, cmdname: str) -> bool:
        import knutbot2.database as db
        s = db.session()
        cooldown = s.query(db.CommandCooldown).filter(db.CommandCooldown.cg_id == chatgroup.cg_id, db.CommandCooldown.command == cmdname).first()
        if not cooldown:
            return False
        user_perm = get_user_permission(user, chatgroup)
        if cooldown.moderator_ignore_cooldown and user_perm & UserPermission.MODERATOR >= UserPermission.MODERATOR:
            return False
        if cooldown.editor_ignore_cooldown and user_perm & UserPermission.EDITOR >= UserPermission.EDITOR:
            return False
        now = datetime.datetime.now(datetime.timezone.utc)
        if cooldown.last_used:
            cooled_down = cooldown.last_used + datetime.timedelta(seconds=cooldown.cooldown)
            if now < cooled_down:
                return True
        cooldown.last_used = now
        s.commit()
        return False

    def has_permission(self, user: User, chatgroup: ChatGroup, cmdname: str) -> bool:
        required_permission = get_cmd_required_permission(cmdname, chatgroup, self.required_permission)
        if required_permission == UserPermission.EVERYONE:
            return True
        return required_permission & get_user_permission(user, chatgroup) >= required_permission


class Command(AbstractCommand):
    def __init__(self,
                 required_permission: UserPermission = UserPermission.EVERYONE,
                 cmdname: Optional[str] = None):
        self.required_permission = required_permission
        self.cmdname = cmdname

    def __call__(self,
                 fn: Callable[..., CommandResponse]) -> Any:
        name = self.cmdname if self.cmdname is not None else fn.__name__

        if asyncio.iscoroutinefunction(fn):
            @functools.wraps(fn)
            async def decorated(*args: Any, **kwargs: Any) -> CommandResponse:
                for a in args:
                    if isinstance(a, CommandParameter):
                        params: CommandParameter = a
                        if self.has_permission(params.user, params.chatgroup, name):
                            if not self.is_on_cooldown(params.user, params.chatgroup, name):
                                return await fn(*args, **kwargs)
                        return CommandResponse(text=[], error=True)
                return CommandResponse(text=[], error=True)

        else:
            @functools.wraps(fn)
            def decorated(*args: Any, **kwargs: Any) -> CommandResponse:
                for a in args:
                    if isinstance(a, CommandParameter):
                        params: CommandParameter = a
                        if self.has_permission(params.user, params.chatgroup, name):
                            if not self.is_on_cooldown(params.user, params.chatgroup, name):
                                return fn(*args, **kwargs)
                        return CommandResponse(text=[], error=True)
                return CommandResponse(text=[], error=True)
        return decorated


class CommonCommand(AbstractCommand):
    """Common Decorator of AsyncCommand and SyncCommand which implements the
    __call__ function for them, the specific classes just pass them
    queue of the evaluator that a CommonCommandWork will be put into"""
    def __init__(self,
                 required_permission: UserPermission,
                 queue: queue.Queue,
                 cmdname: Optional[str]):
        self.required_permission = required_permission
        self.queue = queue
        self.cmdname = cmdname

    def __call__(self,
                 fn: Callable[[CommandParameter], CommandResponse]) -> Callable[[queue.Queue, int, CommandParameter], None]:
        name = self.cmdname if self.cmdname is not None else fn.__name__

        def decorated(answerq: queue.Queue,
                      trans_cache_id: int,
                      params: CommandParameter) -> None:
            if self.has_permission(params.user, params.chatgroup, name):
                if not self.is_on_cooldown(params.user, params.chatgroup, name):
                    self.queue.put(CommonCommandWork(fn, answerq, trans_cache_id, params))
            return None
        return decorated


class CommonAsyncCommand(CommonCommand):

    def __init__(self,
                 required_permission: UserPermission = UserPermission.EVERYONE,
                 cmdname: Optional[str] = None):
        from knutbot2 import evaluator
        super().__init__(required_permission, evaluator.async_queue, cmdname)


class CommonSyncCommand(CommonCommand):

    def __init__(self,
                 required_permission: UserPermission = UserPermission.EVERYONE,
                 cmdname: Optional[str] = None):
        from knutbot2 import evaluator
        super().__init__(required_permission, evaluator.sync_queue, cmdname)


class CommandResponse(object):
    """Class that will be returned by the evaluator into the queue that was
    given by the CommonCommandWork"""
    def __init__(self,
                 text: Union[str, List[str]],
                 error: bool = False,
                 fatal_error: bool = False,
                 trans_cache_id: Optional[int] = None):
        self.text = text if isinstance(text, list) else [text]
        self.error = error
        self.fatal_error = fatal_error
        # fatal error => just remove the entry out of the translator cache
        self.trans_cache_id = trans_cache_id

    def __repr__(self) -> str:
        return f"CommonCommandResponse(text={self.text}, error={self.error})"


class CommandType(enum.Enum):
    COMMON = enum.auto()
    TWITCH = enum.auto()
    DISCORD = enum.auto()


class CommandOrigin(enum.Enum):
    TWITCH = enum.auto()
    DISCORD = enum.auto()
