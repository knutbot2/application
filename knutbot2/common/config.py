import logging
import string
import random
import datetime

from knutbot2.command import (
    CommonAsyncCommand,
    CommonSyncCommand,
    CommandParameter,
    CommandResponse,
    )
from knutbot2.permission import UserPermission
import knutbot2.database as db

@CommonSyncCommand(UserPermission.EDITOR)
def config(param: CommandParameter) -> CommandResponse:
    try:
        subcmd = options[param.args[0]]
    except (IndexError, KeyError):
        return CommandResponse(f"Available options: {' '.join(options)}")
    return subcmd(param)


def config_prefix(param: CommandParameter) -> CommandResponse:
    if len(param.args) == 1:
        attr = getattr(param.chatgroup, param.args[0])
        return CommandResponse(f"Your prefix is {attr}")
    s = db.session()
    s.add(param.chatgroup)
    setattr(param.chatgroup, param.args[0], param.args[1])
    s.commit()
    attr = getattr(param.chatgroup, param.args[0])
    s.close()
    return CommandResponse(f"Changed the prefix to {attr}")


def config_link(param: CommandParameter) -> CommandResponse:
    cg = param.chatgroup
    s = db.session()
    s.add(cg)
    if cg.discordid is not None and cg.twitchid is not None:
        return CommandResponse(text="Both twitch and discord are linked to your chat group already, cannot link more.", error=True)
    if cg.link:
        link = cg.link
        if link.valid_until < datetime.datetime.now(datetime.timezone.utc):
            # set a new token for it
            token = "".join([random.choice(string.ascii_uppercase + string.digits)
                             for _ in range(6)])
            link.token = token
    else:
        link = db.ChatGroupLink()
        token = "".join([random.choice(string.ascii_uppercase + string.digits)
                         for _ in range(6)])
        link.token = token
        link.chatgroup = param.chatgroup
    link.valid_until = datetime.datetime.now(datetime.timezone.utc) + datetime.timedelta(minutes=15)
    s.add(link)
    s.commit()
    return CommandResponse(text=f"Your token: {link.token}")


@CommonSyncCommand(UserPermission.EVERYONE)
def user_link(param: CommandParameter) -> CommandResponse:
    s = db.session()
    user = param.user
    s.add(user)
    if user.discord_id is not None and user.twitch_id is not None:
        return CommandResponse(text="Both twitch and discord are linked to your user already, cannot link more.", error=True)
    if len(param.args) == 0:  # !config link => get new token
        if user.link:
            link = user.link
            if link.valid_until < datetime.datetime.now(datetime.timezone.utc):
                # set a new token for it
                token = "".join([random.choice(string.ascii_uppercase + string.digits)
                                for _ in range(6)])
                link.token = token
        else:
            link = db.UserLink()
            token = "".join([random.choice(string.ascii_uppercase + string.digits)
                            for _ in range(6)])
            link.token = token
            link.user = user
        link.valid_until = datetime.datetime.now(datetime.timezone.utc) + datetime.timedelta(minutes=15)
        s.add(link)
        s.commit()
        return CommandResponse(text=f"Your token: {link.token}")
    else:
        token = param.args[0]
        link = s.query(db.UserLink).filter(db.UserLink.token == token).first()
        if not link:
            return CommandResponse(text="Invalid token.", error=True)
        mergeinto = link.user
        thispermissions = user.user_permissions
        mergeintopermissions = mergeinto.user_permissions
        new_permission = {}
        for perm in thispermissions + mergeintopermissions:
            if perm.cg_id not in new_permission:
                new_permission[perm.cg_id] = perm.userpermission
            else:
                new_permission[perm.cg_id] |= perm.userpermission
            s.delete(perm)
        for k, v in new_permission.items():
            perm = db.UserPermission()
            perm.cg_id = k
            perm.user_id = mergeinto.uid
            perm.userpermission = v
            s.add(perm)
        for cg in user.chatgroups:
            cg.owner = mergeinto
        s.delete(user)
        # flush so the delete is executed before setting the following id
        s.flush()
        if mergeinto.discord_id:
            mergeinto.twitch_id = user.twitch_id
        else:
            mergeinto.discord_id = user.discord_id
        s.delete(link)
        s.commit()
        return CommandResponse(text=f"Users are merged!")


options = {
    'twitchprefix': config_prefix,
    'discordprefix': config_prefix,
    'link': config_link,
    }
