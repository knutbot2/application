import logging
import importlib
from distutils.util import strtobool
from functools import partial
from typing import (
    List,
    )

from knutbot2.exception import CooldownHasNoSecondsError
import knutbot2.database as db
from knutbot2.permission import UserPermission
from knutbot2.command import (
    CommonSyncCommand,
    CommandParameter,
    CommandResponse,
    )


@CommonSyncCommand(UserPermission.MODERATOR)
def cooldown(param: CommandParameter) -> CommandResponse:
    try:
        subcmd = options[param.args[0]]
    except (IndexError, KeyError):
        return CommandResponse(f"Available options: {' '.join(options)}")
    return subcmd(param)


def cooldown_command(param: CommandParameter) -> CommandResponse:
    if len(param.args) < 3:
        return CommandResponse("use: cooldown cmd <commandname> <cooldown in seconds> | moderator_ignore_cooldown/editor_ignore_cooldown yes/no")
    target = param.args[1]
    moderator_ignore_cooldown = None
    editor_ignore_cooldown = None
    seconds = None
    if not param.chatgroup.has_command(target):
        return CommandResponse(f"Command {target} does not exist")
    if param.args[2] == 'moderator_ignore_cooldown':
        try:
            moderator_ignore_cooldown = bool(strtobool(param.args[3]))
        except Exception:
            return CommandResponse(f"use: cooldown cmd {target} moderator_ignore_cooldown yes/no")
    elif param.args[2] == 'editor_ignore_cooldown':
        try:
            editor_ignore_cooldown = bool(strtobool(param.args[3]))
        except Exception:
            return CommandResponse(f"use: cooldown cmd {target} editor_ignore_cooldown yes/no")
    else:
        try:
            seconds = int(param.args[2])
        except Exception:
            return CommandResponse("Cooldown must be in seconds, as integer")
    try:
        cooldown = db.CommandCooldown.set_cooldown(target, param.chatgroup.cg_id,
                                                   seconds=seconds,
                                                   moderator_ignore_cooldown=moderator_ignore_cooldown,
                                                   editor_ignore_cooldown=editor_ignore_cooldown)
    except CooldownHasNoSecondsError as e:
        return CommandResponse(f"Cooldown must be first set with seconds before you can set the {param.args[2]} value")
    return CommandResponse(f"Cooldown for {target} set to {cooldown.cooldown}s, editors ignore it: {cooldown.editor_ignore_cooldown}, moderators ignore it: {cooldown.moderator_ignore_cooldown}")


options = {
    'cmd': cooldown_command,
    #'keyphrase': module_disable,
    }
