from __future__ import annotations
from typing import (
    Optional,
    )
import asyncio

from sqlalchemy.sql.expression import func

from knutbot2.command import (
    Command,
    CommonAsyncCommand,
    CommonSyncCommand,
    CommandParameter,
    CommandResponse,
    CommandType,
    )
from knutbot2.database import (
    session,
    Counter,
    CounterValue,
    Game,
    ChatGroup,
    CG_COMMANDS_MEMBER_TYPE,
    CommandCooldown,
    )
from knutbot2.permission import UserPermission
from knutbot2.twitch import TwitchCommandParameter


def get_counter(chatgroup: ChatGroup, name: str) -> Optional[Counter]:
    session().add(chatgroup)
    return session().query(Counter).filter(Counter.cg_id == chatgroup.cg_id).filter(Counter.name == name).first()


class CustomCounter(object):

    DEFAULT = "Current {name} for {game}: {count} (total count: {total})"

    def __init__(self, name: str):
        self.name = name
        self.add = CommonSyncCommand(UserPermission.MODERATOR, f"add{self.name}")(self._add)
        self.add_twitch = Command(UserPermission.EVERYONE, f"add{self.name}")(self._add_twitch)
        self.get = CommonAsyncCommand(cmdname=f"{self.name}")(self._get)
        self.get_twitch = Command(cmdname=f"{self.name}")(self._get_twitch)
        self.set_msg = CommonSyncCommand(UserPermission.MODERATOR, f"set{self.name}msg")(self._set_msg)

    def get_total(self, counter_id):
        total = session().query(func.sum(CounterValue.count)).\
            filter(CounterValue.counter_id == counter_id).scalar()
        return total if total is not None else 0

    def _commands(self) -> CG_COMMANDS_MEMBER_TYPE:
        return {
            CommandType.COMMON: {
                f"add{self.name}": self.add,
                f"{self.name}": self.get,
                f"set{self.name}msg": self.set_msg,
                },
            CommandType.TWITCH: {
                f"add{self.name}": self.add_twitch,
                f"{self.name}": self.get_twitch,
                },
            }

    def _add(self, param: CommandParameter) -> CommandResponse:
        counter = get_counter(param.chatgroup, self.name)
        if not counter:
            return CommandResponse(f"Counter {self.name} does not exist ... whoop?!",
                                   error=True)
        # Need the "== None" check here, because "is" cannot be overloaded
        v = session().query(CounterValue).\
            filter(CounterValue.counter_id == counter.id).\
            filter(CounterValue.game_id == None).first()
        if v:
            v.count += 1
        else:
            v = CounterValue()
            v.count = 1
            v.counter_id = counter.id
            v.game = None
        session().add(v)
        session().commit()
        total = self.get_total(counter.id)
        rawmsg = counter.message if counter.message else CustomCounter.DEFAULT
        msg = rawmsg.format(name=self.name, count=v.count if v else 0,
                            total=total, game='None')
        return CommandResponse(text=msg)

    async def _add_twitch(self, param: TwitchCommandParameter) -> CommandResponse:
        counter = get_counter(param.chatgroup, self.name)
        if not counter:
            return CommandResponse(f"Counter {self.name} does not exist ... whoop?!",
                                   error=True)
        streaminfo = await param.bot.get_stream(param.message.channel.name)
        if streaminfo:
            gamename = streaminfo['game_name']
        else:
            return self._add(param)
        game = session().query(Game).filter(Game.name == gamename).first()
        if game is None:
            game = Game()
            game.name = gamename
            session().add(game)
            session().flush()
        v = session().query(CounterValue).\
            filter(CounterValue.counter_id == counter.id).\
            filter(CounterValue.game_id == game.id).first()
        if v:
            v.count += 1
        else:
            v = CounterValue()
            v.count = 1
            v.counter_id = counter.id
            v.game = game
        session().add(v)
        session().commit()
        total = self.get_total(counter.id)
        rawmsg = counter.message if counter.message else CustomCounter.DEFAULT
        msg = rawmsg.format(name=self.name, count=v.count if v else 0,
                            total=total, game=game.name)
        return CommandResponse(text=msg)

    def _get(self, param: CommandParameter) -> CommandResponse:
        counter = get_counter(param.chatgroup, self.name)
        if not counter:
            return CommandResponse(f"Counter {self.name} does not exist ... whoop?!",
                                   error=True)
        # Need the "== None" check here, because "is" cannot be overloaded
        v = session().query(CounterValue).\
            filter(CounterValue.counter_id == counter.id).\
            filter(CounterValue.game_id == None).first()
        total = self.get_total(counter.id)
        rawmsg = counter.message if counter.message else CustomCounter.DEFAULT
        msg = rawmsg.format(name=self.name, count=v.count if v else 0,
                            total=total, game='None')
        return CommandResponse(text=msg)

    async def _get_twitch(self, param: TwitchCommandParameter) -> CommandResponse:
        counter = get_counter(param.chatgroup, self.name)
        if not counter:
            return CommandResponse(f"Counter {self.name} does not exist ... whoop?!",
                                   error=True)
        streaminfo = await param.bot.get_stream(param.message.channel.name)
        if streaminfo:
            game = streaminfo['game_name']
        else:
            return self._get(param)
        v = session().query(CounterValue).join(Game).\
            filter(CounterValue.counter_id == counter.id).\
            filter(CounterValue.game_id == Game.id).\
            filter(Game.name == game).first()
        total = self.get_total(counter.id)
        rawmsg = counter.message if counter.message else CustomCounter.DEFAULT
        msg = rawmsg.format(name=self.name, count=v.count if v else 0,
                            total=total, game=game)
        return CommandResponse(text=msg)

    def _set_msg(self, param: CommandParameter) -> CommandResponse:
        counter = get_counter(param.chatgroup, self.name)
        if not counter:
            return CommandResponse(f"Counter {self.name} does not exist ... whoop?!",
                                   error=True)
        msg = ' '.join(param.args)
        if "{count}" and "{total}" not in msg:
            return CommandResponse("Either '{count}' or '{total}' needs to be in the message for it to make any sense ...",
                                   error=True)
        counter.message = msg
        session().commit()
        resp = self._get(param).text[0]
        return CommandResponse(text=f"Changed message. Currently it would look like (without set game): {resp}")


@CommonSyncCommand(UserPermission.EDITOR)
def addcounter(param: CommandParameter) -> CommandResponse:
    try:
        name = param.args[0]
    except IndexError:
        return CommandResponse(text=f"Name of the counter needs to be first argument.",
                                     error=True)
    if not name.isalnum():
        return CommandResponse(text=f"Name needs to be alphanumerical.",
                                     error=True)
    chatgroup = param.chatgroup
    if get_counter(chatgroup, name) is not None:
        return CommandResponse(text=f"Counter {name} exists already.",
                                     error=True)
    counter = Counter()
    counter.name = name
    counter.chatgroup = chatgroup
    chatgroup.add_commands(CustomCounter(name)._commands())
    session().add(counter)
    session().flush()
    CommandCooldown.set_cooldown(f"add{name}", param.chatgroup.cg_id, seconds=5, moderator_ignore_cooldown=False, editor_ignore_cooldown=False)
    return CommandResponse(text=f"Added counter for {name}. Also set up a cooldown for the add method so multiple people adding won't skew the counter.")


@CommonSyncCommand(UserPermission.EDITOR)
def delcounter(param: CommandParameter) -> CommandResponse:
    try:
        name = param.args[0]
    except IndexError:
        return CommandResponse(text=f"Name of the counter needs to be first argument.",
                                     error=True)
    counter = get_counter(param.chatgroup, name)
    if counter is None:
        return CommandResponse(text=f"Counter {name} does not exist.",
                               error=True)
    session().delete(counter)
    session().commit()
    param.chatgroup.del_commands(CustomCounter(name)._commands())
    return CommandResponse(text=f"Counter {name} deleted.",
                           error=True)


def _commands() -> CG_COMMANDS_MEMBER_TYPE:
    return {
        CommandType.COMMON: {
            'addcounter': addcounter,
            'delcounter': delcounter,
            }
        }


def enable(chatgroup: ChatGroup) -> None:
    session().add(chatgroup)
    _cmds = _commands()
    chatgroup.add_commands(_commands())
    for counter in chatgroup.counters:
        chatgroup.add_commands(CustomCounter(counter.name)._commands())


def disable(chatgroup: ChatGroup) -> None:
    chatgroup.del_commands(_commands())
    for counter in chatgroup.counters:
        chatgroup.del_commands(CustomCounter(counter.name)._commands())
