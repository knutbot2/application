from __future__ import annotations
from functools import partial, update_wrapper
from typing import (
    Optional,
    Dict,
    Union,
    Any,
    Tuple,
    Callable,
    TYPE_CHECKING,
    )
import uuid

from sqlalchemy.sql.expression import func
from sqlalchemy.exc import IntegrityError

from knutbot2.database.customtext import (
    CustomTextGroup,
    CustomTextValue,
    CustomtextgroupChatgroupMap,
    )
from knutbot2.command import (
    CommonAsyncCommand,
    CommonSyncCommand,
    CommandParameter,
    CommandResponse,
    CommandType,
    CommonCommandResultType,
    )
from knutbot2.database import (
    session,
    CG_COMMANDS_MEMBER_TYPE,
    )
from knutbot2.permission import UserPermission

if TYPE_CHECKING:
    from knutbot2.database.chatgroup import ChatGroup


def get_textgroup(chatgroup: ChatGroup, name: str) -> Optional[CustomTextGroup]:
    session().add(chatgroup)
    textgroup = None
    for m in chatgroup.textgroupmap:
        if m.name == name:
            tg: CustomTextGroup = m.textgroup
            return tg
    return None


class CustomText(object):

    def __init__(self, name: str):
        self.name = name
        # apparently the self parameter gets only fixed when the Command
        # decorator is applied in the __init__ method
        self.add = CommonSyncCommand(UserPermission.MODERATOR, f"add{self.name}")(self._add)
        self.get = CommonAsyncCommand(cmdname=f"{self.name}")(self._get)
        self.edit = CommonSyncCommand(UserPermission.MODERATOR, f"edit{self.name}")(self._edit)
        self.delete = CommonSyncCommand(UserPermission.MODERATOR, f"del{self.name}")(self._delete)

    def _commands(self) -> CG_COMMANDS_MEMBER_TYPE:
        return {
            CommandType.COMMON: {
                f"add{self.name}": self.add,
                f"{self.name}": self.get,
                f"edit{self.name}": self.edit,
                f"del{self.name}": self.delete,
                }
            }

    def _get(self, param: CommandParameter) -> CommandResponse:
        textgroup = get_textgroup(param.chatgroup, self.name)
        if textgroup is None:
            return CommandResponse(text=f"CustomText {self.name} not found",
                                   error=True)
        try:
            ctv_id: Optional[int] = int(param.args[0])
        except IndexError:
            ctv_id = None
        if ctv_id is not None:
            val = session().query(CustomTextValue).filter(CustomTextValue.ctg_id == textgroup.ctg_id).filter(CustomTextValue.ctv_id == ctv_id).first()
            if val is None:
                return CommandResponse(text=f"Entry {ctv_id} does not exist.",
                                       error=True)
        else:
            val = session().query(CustomTextValue).filter(CustomTextValue.ctg_id == textgroup.ctg_id).order_by(func.random()).limit(1).first()
            if val is None:
                return CommandResponse(text=f"No entries in {self.name}.",
                                             error=True)    
        return CommandResponse(text=f"{val.ctv_value} [{val.ctv_id}]")

    def _add(self, param: CommandParameter) -> CommandResponse:
        textgroup = get_textgroup(param.chatgroup, self.name)
        if textgroup is None:
            return CommandResponse(text=f"CustomText {self.name} not found",
                                         error=True)
        text = ' '.join(param.args)
        if not text:
            return CommandResponse(text=f"Cannot add empty entry",
                                         error=True)
        max_ctv_id = textvalue = session().query(func.max(CustomTextValue.ctv_id)).filter(CustomTextValue.ctg_id == textgroup.ctg_id).first()[0]
        val = CustomTextValue()
        val.textgroup = textgroup
        val.ctv_id = max_ctv_id + 1 if max_ctv_id else 1
        val.ctv_value = text
        session().add(val)
        try:
            session().commit()
        except IntegrityError as e:
            session().rollback()
            val = session().query(CustomTextValue).filter(CustomTextValue.ctv_value == text).first()
            return CommandResponse(text=f"{self.name} exists already with the id {val.ctv_id}.", error=True)
        return CommandResponse(text=f"Added {self.name} with id {val.ctv_id}")

    def _edit(self, param: CommandParameter) -> CommandResponse:
        textgroup = get_textgroup(param.chatgroup, self.name)
        if textgroup is None:
            return CommandResponse(text=f"CustomText {self.name} not found.",
                                         error=True)
        try:
            ctv_id = int(param.args[0])
        except ValueError:
            return CommandResponse(text=f"First argument needs to be the id to be edited.",
                                         error=True)
        text = ' '.join(param.args[1:])
        if not text:
            return CommandResponse(text=f"Cannot set it to an empty entry.",
                                         error=True)
        val = session().query(CustomTextValue).filter(CustomTextValue.ctv_id == ctv_id).first()
        if val is None:
            return CommandResponse(text=f"Entry {ctv_id} does not exist.",
                                         error=True)
        val.ctv_value = text
        session().commit()
        return CommandResponse(text=f"Edited {self.name} with id {ctv_id}.")

    def _delete(self, param: CommandParameter) -> CommandResponse:
        textgroup = get_textgroup(param.chatgroup, self.name)
        if textgroup is None:
            return CommandResponse(text=f"CustomText {self.name} not found.",
                                         error=True)
        try:
            ctv_id = int(param.args[0])
        except ValueError:
            return CommandResponse(text=f"First argument needs to be the id to be deleted.",
                                         error=True)
        val = session().query(CustomTextValue).filter(CustomTextValue.ctv_id == ctv_id).first()
        if val is None:
            return CommandResponse(text=f"Entry {ctv_id} does not exist.",
                                         error=True)
        session().delete(val)
        session().commit()
        return CommandResponse(text=f"Deleted {self.name} with id {ctv_id}.")


@CommonSyncCommand(UserPermission.EDITOR, 'addcustomtext')
def add(param: CommandParameter) -> CommandResponse:
    try:
        name = param.args[0]
    except IndexError:
        return CommandResponse(text=f"Name of the customtext needs to be first argument.",
                                     error=True)
    if not name.isalnum():
        return CommandResponse(text=f"Name needs to be alphanumerical.",
                                     error=True)
    chatgroup = param.chatgroup
    if get_textgroup(chatgroup, name) is not None:
        return CommandResponse(text=f"Customtext {name} exists already.",
                                     error=True)
    shared = len(param.args) > 1
    if shared:
        try:
            uid = uuid.UUID(param.args[1])
        except ValueError:
            return CommandResponse(text=f"'{param.args[1]}' is not a valid id for a shared custom text.",
                                         error=True)
        ctg = session().query(CustomTextGroup).filter(CustomTextGroup.ctg_id == uid).first()
        if ctg is None:
            return CommandResponse(text=f"Custom text with id '{param.args[1]}' does not exist.",
                                         error=True)
    else:
        ctg = CustomTextGroup()
    ctm = CustomtextgroupChatgroupMap()
    ctm.name = name
    ctm.chatgroup = chatgroup
    ctm.textgroup = ctg
    session().add(ctm)
    session().commit()
    chatgroup.add_commands(CustomText(name)._commands())
    return CommandResponse(text=f"Added {'shared' if shared else ''}custom texts as {name}.")


@CommonAsyncCommand(UserPermission.EDITOR, 'customtextid')
def ctid(param: CommandParameter) -> CommandResponse:
    try:
        name = param.args[0]
    except KeyError:
        return CommandResponse(text=f"Name of the customtext needs to be first argument.",
                                     error=True)
    chatgroup = param.chatgroup
    textgroup = get_textgroup(chatgroup, name)
    if textgroup is None:
        return CommandResponse(text=f"Customtext {name} does not exist.",
                                     error=True)
    return CommandResponse(text=f"The id is {textgroup.ctg_id}")


@CommonSyncCommand(UserPermission.EDITOR, 'delcustomtext')
def delete(param: CommandParameter) -> CommandResponse:
    try:
        name = param.args[0]
    except KeyError:
        return CommandResponse(text=f"Name of the customtext needs to be first argument.",
                                     error=True)
    chatgroup = param.chatgroup
    session().add(chatgroup)
    for m in chatgroup.textgroupmap:
        if m.name == name:
            textgroup: CustomTextGroup = m.textgroup
            chatgroup.del_commands(CustomText(m.name)._commands())
            session().delete(m)
            session().flush()
            if len(textgroup.chatgroupmap) == 0:
                session().delete(textgroup)
            session().commit()
            return CommandResponse(text=f"Customtext {name} deleted.",
                                         error=True)
    return CommandResponse(text=f"Customtext {name} does not exist.",
                                 error=True)


def _commands() -> CG_COMMANDS_MEMBER_TYPE:
    return {
        CommandType.COMMON: {
            'addcustomtext': add,
            'delcustomtext': delete,
            'customtextid': ctid,
            }
        }


def enable(chatgroup: ChatGroup) -> None:
    session().add(chatgroup)
    _cmds = _commands()
    chatgroup.add_commands(_commands())
    for tgm in chatgroup.textgroupmap:
        chatgroup.add_commands(CustomText(tgm.name)._commands())


def disable(chatgroup: ChatGroup) -> None:
    chatgroup.del_commands(_commands())
    for tgm in chatgroup.textgroupmap:
        chatgroup.del_commands(CustomText(tgm.name)._commands())
