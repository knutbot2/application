import logging
import importlib
from functools import partial
from typing import (
    List,
    )

from knutbot2 import database
from knutbot2.permission import UserPermission
from knutbot2.command import (
    CommonSyncCommand,
    CommandParameter,
    CommandResponse,
    )


@CommonSyncCommand(UserPermission.EDITOR)
def module(param: CommandParameter) -> CommandResponse:
    try:
        subcmd = options[param.args[0]]
    except (IndexError, KeyError):
        return CommandResponse(f"Available options: {' '.join(options)}")
    return subcmd(param)


def module_enable(param: CommandParameter) -> CommandResponse:
    mod = ".".join(param.args[1:])
    if mod in ['common.config',
               'common.module',
               'common.permission',
               'common.cooldown']:
        return CommandResponse(text="Hats off, nice try, you checked the source code. But sorry, that module is always on.")
    s = database.session()
    s.add(param.chatgroup)
    cgm = s.query(database.ChatGroupModule).filter(database.ChatGroupModule.cg_id == param.chatgroup.cg_id, database.ChatGroupModule.name == mod).first()
    if cgm:
        return CommandResponse(text="Module already enabled.", error=True)
    msplit = mod.split('.')[:2]
    try:
        m = importlib.import_module(f".{msplit[1]}", f'knutbot2.{msplit[0]}')
    except (IndexError, ModuleNotFoundError) as e:
        logging.exception("Exception importing module")
        return CommandResponse(text=f"Module {'.'.join(msplit)} does not exist.", error=True)
    m.enable(param.chatgroup) # type: ignore
    cgm = database.ChatGroupModule()
    cgm.name = '.'.join(msplit)
    cgm.chatgroup = param.chatgroup
    s.commit()
    return CommandResponse(text=f"Module enabled.")


def module_disable(param: CommandParameter) -> CommandResponse:
    mod = ".".join(param.args[1:])
    if mod in ['common.config', 'common.module']:
        return CommandResponse(text="Hats off, nice try, you checked the source code. But sorry, that module is always on.")
    s = database.session()
    s.add(param.chatgroup)
    cgm = s.query(database.ChatGroupModule).filter(database.ChatGroupModule.cg_id == param.chatgroup.cg_id, database.ChatGroupModule.name == mod).first()
    if not cgm:
        return CommandResponse(text="Module already disabled.", error=True)
    msplit = mod.split('.')
    try:
        m = importlib.import_module(f".{msplit[1]}", f'knutbot2.{msplit[0]}')
    except (IndexError, ModuleNotFoundError):
        return CommandResponse(text=f"Module {'.'.join(msplit)} does not exist.", error=True)
    m.disable(param.chatgroup) # type: ignore
    s.delete(cgm)
    s.commit()
    return CommandResponse(text=f"Module disabled.")


options = {
    'enable': module_enable,
    'disable': module_disable,
    }
