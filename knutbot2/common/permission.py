from typing import Optional

from knutbot2.discord.util import get_user_from_mention_string
from knutbot2.command import (
    CommonAsyncCommand,
    CommonSyncCommand,
    CommandParameter,
    CommandResponse,
    CommandOrigin,
    )
from knutbot2.permission import UserPermission
import knutbot2.database as db


@CommonSyncCommand(UserPermission.MODERATOR)
def permission(param: CommandParameter) -> CommandResponse:
    try:
        subcmd = options[param.args[0]]
    except (IndexError, KeyError):
        return CommandResponse(f"Available options: {' '.join(options)}")
    return subcmd(param)


def permission_user(param: CommandParameter) -> CommandResponse:
    if len(param.args) < 3:
        return CommandResponse("use: permission user <user> <permission>")
    target = param.args[1]
    try:
        level = UserPermission[param.args[2].upper()]
        if level == UserPermission.OWNER:
            return CommandResponse(f"Cannot set permission level to {level.name}")
    except Exception:
        return CommandResponse("Permission level does not exist")
    session = db.session()
    user: db.User
    if param.origin == CommandOrigin.TWITCH:
        if target.startswith('@'):
            target = target[1:]
        tu = session.query(db.TwitchUser).filter(db.TwitchUser.username == target.lower()).first()
        if tu:
            if tu.user is None:
                return CommandResponse(f"User not found")
            user = tu.user
        else:
            return CommandResponse(f"User not found")
    elif param.origin == CommandOrigin.DISCORD:
        u = get_user_from_mention_string(target)
        if u is None:
            try:
                name, discr = target.split('#')
            except Exception:
                return CommandResponse(f"User not found")
            du = session.query(db.DiscordUser).filter(db.DiscordUser.username == name).filter(db.DiscordUser.discriminator == discr).first()
            if du:
                user = du.user
            else:
                return CommandResponse(f"User not found")
        else:
            user = u
    perm = session.query(db.UserPermission).filter(db.UserPermission.user_id == user.uid).filter(db.UserPermission.cg_id == param.chatgroup.cg_id).first()
    if not perm:
        perm = db.UserPermission()
        session.add(perm)
        perm.cg_id = param.chatgroup.cg_id
        perm.user_id = user.uid
    perm.userpermission = level.value
    session.commit()
    return CommandResponse(f"Permission for {target} set to {level.name}")


def permission_cmd(param: CommandParameter) -> CommandResponse:
    if len(param.args) < 3:
        return CommandResponse("use: permission cmd <commandname> <permission>")
    target = param.args[1]
    try:
        level = UserPermission[param.args[2].upper()]
        if level == UserPermission.OWNER:
            return CommandResponse(f"Cannot set permission level to {level.name}")
    except Exception:
        return CommandResponse("Permission level does not exist")
    if not param.chatgroup.has_command(target):
        return CommandResponse(f"Command {target} does not exist")
    if db.CommandPermission.set_cmd_permission(target, param.chatgroup.cg_id, level):
        return CommandResponse(f"Permission for {target} set to {level.name}")
    else:
        return CommandResponse(f"There was an error setting the permission, contact your hoster please")


options = {
    'user': permission_user,
    'cmd': permission_cmd,
    }
