from __future__ import annotations
import datetime

from sqlalchemy.sql.expression import func

from knutbot2.command import (
    CommonAsyncCommand,
    CommonSyncCommand,
    CommandParameter,
    CommandResponse,
    CommandType,
    )
from knutbot2.database import (
    session,
    Quote,
    ChatGroup,
    CG_COMMANDS_MEMBER_TYPE,
    )
from knutbot2.permission import UserPermission


@CommonSyncCommand(UserPermission.MODERATOR, 'addquote')
def add(param: CommandParameter) -> CommandResponse:
    if len(param.args) < 2:
        return CommandResponse(text=f"addquote <quoted> quote ...")
    name = param.args[0]
    if not name.isalnum():
        return CommandResponse(text=f"Name needs to be alphanumerical.",
                                     error=True)
    chatgroup = param.chatgroup
    session().add(chatgroup)
    max_id = session().query(func.max(Quote.id)).filter(Quote.cg_id == chatgroup.cg_id).first()[0]
    q = Quote()
    q.cg_id = chatgroup.cg_id
    q.id = max_id+1 if max_id else 1
    q.quoted = name
    q.quote = ' '.join(param.args[1:])
    q.timestamp = datetime.datetime.now(datetime.timezone.utc)
    session().add(q)
    session().commit()
    return CommandResponse(text=f"Added quote of {name}.")


@CommonAsyncCommand(UserPermission.EVERYONE, 'quote')
def get(param: CommandParameter) -> CommandResponse:
    try:
        qid = int(param.args[0])
    except (IndexError, TypeError):
        qid = None
    chatgroup = param.chatgroup
    session().add(chatgroup)
    query = session().query(Quote).filter(Quote.cg_id == chatgroup.cg_id)
    if qid is not None:
        query = query.filter(Quote.id == qid)
        q = query.first()
        if q is None:
            return CommandResponse(text=f"Quote with id {qid} does not exist.",
                                   error=True)
    else:
        query = query.order_by(func.random()).limit(1)
        q = query.first()
        if q is None:
            return CommandResponse(text=f"No quotes were added yet.",
                                   error=True)
    timestr = q.timestamp.strftime("%Y-%m-%d")
    return CommandResponse(text=f"({q.id}) {q.quoted}: \"{q.quote}\" [{timestr}]")


@CommonSyncCommand(UserPermission.MODERATOR, 'delquote')
def delete(param: CommandParameter) -> CommandResponse:
    try:
        qid = int(param.args[0])
    except (IndexError, TypeError):
        return CommandResponse(text=f"Quote id must be the first argument.",
                               error=True)
    chatgroup = param.chatgroup
    session().add(chatgroup)
    q = session().query(Quote).filter(Quote.cg_id == chatgroup.cg_id).filter(Quote.id == qid).first()
    if not q:
        return CommandResponse(text=f"Quote with id {qid} does not exist.",
                               error=True)
    name = q.quoted
    session().delete(q)
    session().commit()
    return CommandResponse(text=f"Deleted quote with id {qid}, which was from {name}.")


@CommonSyncCommand(UserPermission.MODERATOR)
def editquote(param: CommandParameter) -> CommandResponse:
    try:
        qid = int(param.args[0])
        name = param.args[1]
        quote = ' '.join(param.args[2:])
        if not quote:
            raise IndexError()
    except (IndexError, TypeError):
        return CommandResponse(text=f"Usage: editquote <id> <quoted> <quote ...>",
                               error=True)
    chatgroup = param.chatgroup
    session().add(chatgroup)
    q = session().query(Quote).filter(Quote.cg_id == chatgroup.cg_id).filter(Quote.id == qid).first()
    if not q:
        return CommandResponse(text=f"Quote with id {qid} does not exist.",
                               error=True)
    old = q.quoted
    q.quoted = name
    q.quote = quote
    session().commit()
    return CommandResponse(text=f"Edited quote with id {qid}, which was from {old} and is now from {name}.")


def _commands() -> CG_COMMANDS_MEMBER_TYPE:
    return {
        CommandType.COMMON: {
            'addquote': add,
            'quote': get,
            'delquote': delete,
            'editquote': editquote,
            }
        }


def enable(chatgroup: ChatGroup) -> None:
    chatgroup.add_commands(_commands())


def disable(chatgroup: ChatGroup) -> None:
    chatgroup.del_commands(_commands())

