from __future__ import annotations

import importlib
import queue
import datetime
from typing import (
    Dict,
    Callable,
    Union,
    Any,
    Mapping,
    Tuple,
    List,
    Coroutine,
    Optional,
    TYPE_CHECKING,
    )
from functools import partial

import sqlalchemy
import sqlalchemy.orm
from sqlalchemy.schema import UniqueConstraint

from knutbot2 import (
    database,
    exception,
    )
from knutbot2.database.user import User
from knutbot2.command import (
    CommonCommand,
    CommandParameter,
    CommandResponse,
    CommonAsyncCommand,
    CommonSyncCommand,
    CommandType,
    CommonCommandResultType,
    )
from knutbot2.common import (
    config as cgconfig,
    module,
    permission,
    cooldown,
    )

if TYPE_CHECKING:
    from knutbot2.discord import DiscordReactionParameter

CG_COMMANDS_TYPE = Union[CommonCommandResultType, Callable[..., Any]]
CG_COMMANDS_MEMBER_TYPE = Dict[CommandType, Dict[str, CG_COMMANDS_TYPE]]

###############################
# Methods for empty ChatGroup #
###############################

@CommonSyncCommand(cmdname='help')
def _empty_help(param: CommandParameter) -> CommandResponse:
    return CommandResponse(text=[
        "New to the bot? Use !new to create a new chat group. "
        "Want to link your existing chat group to this one? Use !link"
        ])


@CommonSyncCommand(cmdname='new')
def _empty_new(param: CommandParameter) -> CommandResponse:
    cg = ChatGroup(owner=param.chatgroup.owner,
                   twitchid=param.chatgroup.twitchid,
                   discordid=param.chatgroup.discordid)
    s = database.session()
    s.add(cg)
    s.commit()
    return CommandResponse(text=[
        f"Chat group created!"
        ])


@CommonSyncCommand(cmdname='link')
def _empty_link(param: CommandParameter) -> CommandResponse:
    try:
        token = param.args[0]
    except IndexError:
        return CommandResponse(text="Need to provide link token. Go to your existing chat group and use !config link", error=True)
    s = database.session()
    link = s.query(ChatGroupLink).filter(ChatGroupLink.token == token).first()
    if not link or datetime.datetime.now(datetime.timezone.utc) > link.valid_until:
        if link:
            s.delete(link)
            s.commit()
        return CommandResponse(text="Token invalid or expired, try again")
    if param.chatgroup.discordid is not None and link.chatgroup.discordid is None:
        link.chatgroup.discordid = param.chatgroup.discordid
    elif param.chatgroup.twitchid is not None and link.chatgroup.twitchid is None:
        link.chatgroup.twitchid = param.chatgroup.twitchid
    else:
        return CommandResponse(text="Your chat group seems to be linked already", error=True)
    s.delete(link)
    s.commit()
    return CommandResponse(text="Linked the chat group", error=True)


class ChatGroup(database.Base):
    __tablename__ = 'chatgroup'

    cg_id = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True)
    owner_uid = sqlalchemy.Column(sqlalchemy.BigInteger, sqlalchemy.ForeignKey('users.uid'))
    discordid = sqlalchemy.Column(sqlalchemy.BigInteger, autoincrement=False)
    twitchid = sqlalchemy.Column(sqlalchemy.BigInteger, autoincrement=False)
    twitchprefix = sqlalchemy.Column(sqlalchemy.String(4), default='!')
    discordprefix = sqlalchemy.Column(sqlalchemy.String(4), default='!')

    owner = sqlalchemy.orm.relationship('User', backref='chatgroups')
    modules = sqlalchemy.orm.relationship('ChatGroupModule',
                                          back_populates='chatgroup',
                                          cascade="all, delete",
                                          passive_deletes=True)

    #commands: Dict[
        #str,
        #Tuple[
            #CommandType,
            #Union[
                #CommonCommandResultType,
                #Callable[
                    #...,
                    #Any
                    #]
                #]
            #]
        #]
    commands: CG_COMMANDS_MEMBER_TYPE

    @sqlalchemy.orm.reconstructor
    def __on_db_init__(self) -> None:
        self.commands = self.init_commands()
        self.discord_reaction_callbacks: List[Callable[[DiscordReactionParameter], Coroutine[Any, Any, bool]]] = []
        for mod in self.modules:
            msplit = mod.name.split('.')
            m = importlib.import_module(f".{msplit[1]}", f'knutbot2.{msplit[0]}')
            m.enable(self)  # type: ignore

    def init_commands(self) -> CG_COMMANDS_MEMBER_TYPE:
        cmds: CG_COMMANDS_MEMBER_TYPE = {ct: {} for ct in CommandType}
        cmds[CommandType.COMMON].update({
                'help': CommonAsyncCommand(cmdname='help')(lambda p: CommandResponse("Help soon")),
                'config': cgconfig.config,
                'userlink': cgconfig.user_link,
                'module': module.module,
                'permission': permission.permission,
                'cooldown': cooldown.cooldown,
                })
        return cmds

    def get_command(self, origin: CommandType, name: str) -> Optional[Tuple[CommandType, CG_COMMANDS_TYPE]]:
        if name in self.commands[origin]:
            return origin, self.commands[origin][name]
        if name in self.commands[CommandType.COMMON]:
            return CommandType.COMMON, self.commands[CommandType.COMMON][name]
        return None

    def add_commands(self, commands: CG_COMMANDS_MEMBER_TYPE) -> None:
        for ct in commands:
            for name in commands[ct]:
                if name in self.commands[ct]:
                    raise exception.CommandNameAlreadyExists(f"Command {name} already exists")
        for ct in commands:
            for name in commands[ct]:
                self.commands[ct][name] = commands[ct][name]

    def del_commands(self, commands: CG_COMMANDS_MEMBER_TYPE) -> None:
        for ct in commands:
            for name in commands[ct]:
                try:
                    del self.commands[ct][name]
                except KeyError:
                    pass

    def has_command(self, name: str, origin: Optional[CommandType] = None) -> bool:
        if origin is not None:
            return name in self.commands[origin]
        else:
            for ct in self.commands:
                if name in self.commands[ct]:
                    return True
            return False

    @staticmethod
    def init_empty_commands() -> CG_COMMANDS_MEMBER_TYPE:
        cmd: CG_COMMANDS_MEMBER_TYPE = {ct: {} for ct in CommandType}
        cmd[CommandType.COMMON].update({
            'help': _empty_help,
            'new': _empty_new,
            'link': _empty_link,
            })
        return cmd

    @staticmethod
    def create_empty(owner: User, twitchid: int = None, discordid: int = None) -> ChatGroup:
        # create an unique id -> let's use the negative of the owner
        cg = ChatGroup(cg_id=-owner.uid,
                       twitchid=twitchid,
                       discordid=discordid,
                       twitchprefix='!',
                       discordprefix='!')
        cg.owner = owner
        cg.commands = ChatGroup.init_empty_commands()
        return cg


class ChatGroupModule(database.Base):
    __tablename__ = 'chatgroup_module'

    cgm_id = sqlalchemy.Column(sqlalchemy.BigInteger,
                               primary_key=True)
    cg_id = sqlalchemy.Column(sqlalchemy.Integer,
                              sqlalchemy.ForeignKey('chatgroup.cg_id',
                                                    ondelete="CASCADE",),)
    name = sqlalchemy.Column(sqlalchemy.String(32),
                             nullable=False)
    config = sqlalchemy.Column(sqlalchemy.Text)

    chatgroup = sqlalchemy.orm.relationship('ChatGroup',
                                            back_populates='modules')

    __table_args__ = (UniqueConstraint('cg_id', 'name'),)


class ChatGroupLink(database.Base):
    __tablename__ = 'chatgroup_link'

    cg_id = sqlalchemy.Column(sqlalchemy.Integer,
                              sqlalchemy.ForeignKey('chatgroup.cg_id',
                                                    ondelete="CASCADE",),
                              unique=True,
                              nullable=False)
    token = sqlalchemy.Column(sqlalchemy.CHAR(6),
                              nullable=False,
                              primary_key=True)
    valid_until = sqlalchemy.Column(sqlalchemy.DateTime(True),
                                    nullable=False)
    
    chatgroup = sqlalchemy.orm.relationship('ChatGroup',
                                            backref=sqlalchemy.orm.backref("link", uselist=False))
