import datetime
from typing import (
    Optional,
    )

import sqlalchemy
import sqlalchemy.schema

from knutbot2 import (
    database as db,
    exception,
    )
from knutbot2.database.base import Base


class CommandCooldown(Base):
    __tablename__ = 'cooldown_command'

    id = sqlalchemy.Column(sqlalchemy.BigInteger, primary_key=True)
    command = sqlalchemy.Column(sqlalchemy.String(32), nullable=False)
    cg_id = sqlalchemy.Column(sqlalchemy.Integer,
                              sqlalchemy.ForeignKey('chatgroup.cg_id'),
                              nullable=False)
    cooldown = sqlalchemy.Column(sqlalchemy.Integer, nullable=False)
    moderator_ignore_cooldown = sqlalchemy.Column(sqlalchemy.Boolean,
                                                  nullable=False,
                                                  default=False)
    editor_ignore_cooldown = sqlalchemy.Column(sqlalchemy.Boolean,
                                               nullable=False,
                                               default=True)
    last_used: datetime.datetime = sqlalchemy.Column(sqlalchemy.DateTime(True))

    chatgroup = sqlalchemy.orm.relationship('ChatGroup', backref='command_cooldowns')

    @staticmethod
    def set_cooldown(cmdname: str,
                     chatgroup_id: int,
                     *,
                     seconds: Optional[int] = None,
                     moderator_ignore_cooldown: Optional[bool] = None,
                     editor_ignore_cooldown: Optional[bool] = None,):
        session = db.session()
        cooldown = session.query(CommandCooldown).filter(CommandCooldown.command == cmdname).filter(CommandCooldown.cg_id == chatgroup_id).first()
        if not cooldown:
            if not seconds:
                raise exception.CooldownHasNoSecondsError()
            cooldown = CommandCooldown()
            session.add(cooldown)
            cooldown.cg_id = chatgroup_id
            cooldown.command = cmdname
        if moderator_ignore_cooldown is not None:
            cooldown.moderator_ignore_cooldown = moderator_ignore_cooldown
        if editor_ignore_cooldown is not None:
            cooldown.editor_ignore_cooldown = editor_ignore_cooldown
        if seconds is not None:
            cooldown.cooldown = seconds
        session.commit()
        return cooldown

sqlalchemy.schema.Index("idx_cmd_cd__cg_id__command", CommandCooldown.cg_id, CommandCooldown.command)
