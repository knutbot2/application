from __future__ import annotations

import sqlalchemy
import sqlalchemy.orm

from knutbot2 import database


class Counter(database.Base):
    __tablename__ = 'counter'

    id = sqlalchemy.Column(sqlalchemy.BigInteger,
                           primary_key=True,
                           )
    cg_id = sqlalchemy.Column(sqlalchemy.Integer,
                              sqlalchemy.ForeignKey('chatgroup.cg_id',
                                                    ondelete="CASCADE",),
                              index=True,
                              nullable=False,
                              )
    name = sqlalchemy.Column(sqlalchemy.String(32),
                             nullable=False,
                             )
    message = sqlalchemy.Column(sqlalchemy.String(256))

    chatgroup = sqlalchemy.orm.relationship('ChatGroup', backref='counters')
    values = sqlalchemy.orm.relationship('CounterValue',
                                         back_populates='counter',
                                         cascade="delete, merge, save-update",
                                         )


class CounterValue(database.Base):
    __tablename__ = 'counter_value'

    id = sqlalchemy.Column(sqlalchemy.BigInteger,
                           primary_key=True)
    counter_id = sqlalchemy.Column(sqlalchemy.BigInteger,
                                   sqlalchemy.ForeignKey('counter.id',
                                                         ondelete='CASCADE',),
                                   nullable=False,
                                   index=True)
    game_id = sqlalchemy.Column(sqlalchemy.Integer,
                                sqlalchemy.ForeignKey('game.id'))
    count = sqlalchemy.Column(sqlalchemy.Integer,
                              nullable=False)

    game = sqlalchemy.orm.relationship('Game', backref='game', uselist=False)
    counter = sqlalchemy.orm.relationship('Counter', 
                                          back_populates='values',
                                          )
