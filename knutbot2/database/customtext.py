from __future__ import annotations

import uuid
from typing import (
    Dict,
    Callable,
    Union,
    Any
    )

import sqlalchemy
import sqlalchemy.orm
from sqlalchemy.schema import UniqueConstraint
from sqlalchemy.dialects.postgresql import UUID

from knutbot2 import database


class CustomtextgroupChatgroupMap(database.Base):
    __tablename__ = 'customtextgroup_chatgroup_map'

    cg_id = sqlalchemy.Column(sqlalchemy.Integer,
                              sqlalchemy.ForeignKey('chatgroup.cg_id',
                                                    ondelete="CASCADE",),
                              primary_key=True,)
    ctg_id = sqlalchemy.Column(UUID(as_uuid=True),
                               sqlalchemy.ForeignKey('customtextgroup.ctg_id'),
                               primary_key=True)
    name = sqlalchemy.Column(sqlalchemy.String(32),
                             nullable=False)

    chatgroup = sqlalchemy.orm.relationship('ChatGroup',
                                            backref='textgroupmap')
    textgroup = sqlalchemy.orm.relationship('CustomTextGroup',
                                            backref='chatgroupmap')


class CustomTextGroup(database.Base):
    __tablename__ = 'customtextgroup'

    ctg_id = sqlalchemy.Column(UUID(as_uuid=True),
                               primary_key=True,
                               default=uuid.uuid4)
    values = sqlalchemy.orm.relationship('CustomTextValue',
                                         back_populates='textgroup',
                                         cascade="all, delete",
                                         passive_deletes=True)


class CustomTextValue(database.Base):
    __tablename__ = 'customtextvalue'

    ctg_id = sqlalchemy.Column(UUID(as_uuid=True),
                               sqlalchemy.ForeignKey('customtextgroup.ctg_id',
                                                     ondelete="CASCADE",
                                                     ),
                               nullable=False,
                               primary_key=True,
                               )
    ctv_id = sqlalchemy.Column(sqlalchemy.Integer,
                               nullable=False,
                               primary_key=True,
                               autoincrement=False,
                               )
    ctv_value = sqlalchemy.Column(sqlalchemy.String(400))

    textgroup = sqlalchemy.orm.relationship('CustomTextGroup',
                                            back_populates="values")

    __table_args__ = (UniqueConstraint('ctg_id', 'ctv_value'),)
