import sqlalchemy

from knutbot2.database.base import Base


class DiscordUser(Base):
    __tablename__ = 'users_discord'

    did = sqlalchemy.Column(sqlalchemy.BigInteger, primary_key=True, autoincrement=False)
    username = sqlalchemy.Column(sqlalchemy.String(64))
    discriminator = sqlalchemy.Column(sqlalchemy.types.CHAR(4))
