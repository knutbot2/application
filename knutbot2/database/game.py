from __future__ import annotations

import sqlalchemy

from knutbot2 import database


class Game(database.Base):
    __tablename__ = 'game'

    id = sqlalchemy.Column(sqlalchemy.BigInteger,
                           primary_key=True,
                           )
    name = sqlalchemy.Column(sqlalchemy.String(128),
                             nullable=False,
                             index=True,
                             )
