import sqlalchemy
import logging

from knutbot2 import database as db
from knutbot2.database.base import Base
from knutbot2.permission import UserPermission


class UserPermission(Base):
    __tablename__ = 'user_permission'

    permissionid = sqlalchemy.Column(sqlalchemy.BigInteger, primary_key=True)
    user_id = sqlalchemy.Column(sqlalchemy.BigInteger, sqlalchemy.ForeignKey('users.uid'))
    cg_id = sqlalchemy.Column(sqlalchemy.Integer,
                              sqlalchemy.ForeignKey('chatgroup.cg_id'),
                              nullable=False)
    userpermission = sqlalchemy.Column(sqlalchemy.Integer,
                                       autoincrement=False,
                                       nullable=False)

    user = sqlalchemy.orm.relationship('User', backref='user_permissions')
    chatgroup = sqlalchemy.orm.relationship('ChatGroup', backref='user_permissions')


class CommandPermission(Base):
    __tablename__ = 'command_permission'

    permissionid = sqlalchemy.Column(sqlalchemy.BigInteger, primary_key=True)
    cg_id = sqlalchemy.Column(sqlalchemy.Integer,
                              sqlalchemy.ForeignKey('chatgroup.cg_id'),
                              nullable=False)
    command_name = sqlalchemy.Column(sqlalchemy.String(32), nullable=False)
    required_permission = sqlalchemy.Column(sqlalchemy.Integer,
                                            autoincrement=False,
                                            nullable=False)

    chatgroup = sqlalchemy.orm.relationship('ChatGroup', backref='command_permissions')

    @staticmethod
    def set_cmd_permission(cmdname: str, chatgroup_id: int, level: UserPermission):
        session = db.session()
        try:
            perm = session.query(db.CommandPermission).filter(db.CommandPermission.command_name == cmdname).filter(db.CommandPermission.cg_id == chatgroup_id).first()
            if not perm:
                perm = db.CommandPermission()
                session.add(perm)
                perm.cg_id = chatgroup_id
                perm.command_name = cmdname
            perm.required_permission = level.value
            session.commit()
            return True
        except Exception as e:
            logging.exception("Error trying to set the command permission")
            session.rollback()
            return False

sqlalchemy.schema.Index("idx_userpermission__cg_id__user_id", UserPermission.cg_id, UserPermission.user_id)
sqlalchemy.schema.Index("idx_commandpermission__cg_id__command", CommandPermission.cg_id, CommandPermission.command_name)
