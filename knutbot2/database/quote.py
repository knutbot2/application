from __future__ import annotations

import uuid
from typing import (
    Dict,
    Callable,
    Union,
    Any
    )

import sqlalchemy
import sqlalchemy.orm
from sqlalchemy.schema import UniqueConstraint
from sqlalchemy.dialects.postgresql import UUID

from knutbot2 import database


class Quote(database.Base):
    __tablename__ = 'quote'

    cg_id = sqlalchemy.Column(sqlalchemy.Integer,
                              sqlalchemy.ForeignKey('chatgroup.cg_id',
                                                    ondelete="CASCADE",),
                              primary_key=True,)
    id = sqlalchemy.Column(sqlalchemy.Integer,
                           primary_key=True,
                           autoincrement=False)
    quoted = sqlalchemy.Column(sqlalchemy.String(32),
                               nullable=False)
    quote = sqlalchemy.Column(sqlalchemy.String(400),
                              nullable=False)
    timestamp = sqlalchemy.Column(sqlalchemy.DateTime(True),
                                  nullable=False)
