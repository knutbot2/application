import sqlalchemy

from knutbot2.database.base import Base


class TwitchChannel(Base):
    __tablename__ = 'twitchchannel'

    twitchid = sqlalchemy.Column(sqlalchemy.BigInteger,
                                 primary_key=True,
                                 autoincrement=False)

