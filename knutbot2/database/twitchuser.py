import sqlalchemy

from knutbot2.database.base import Base


class TwitchUser(Base):
    __tablename__ = 'users_twitch'

    tid = sqlalchemy.Column(sqlalchemy.BigInteger, primary_key=True, autoincrement=False)
    username = sqlalchemy.Column(sqlalchemy.String(64))
    displayname = sqlalchemy.Column(sqlalchemy.String(64))

