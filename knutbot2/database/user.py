import sqlalchemy
import sqlalchemy.orm
import sqlalchemy.types
import datetime

from knutbot2 import util
from knutbot2.database import Base
from knutbot2.database.twitchuser import TwitchUser
from knutbot2.database.discorduser import DiscordUser


class User(Base):
    __tablename__ = 'users'

    uid: int = sqlalchemy.Column(sqlalchemy.BigInteger, primary_key=True)
    username: str = sqlalchemy.Column(sqlalchemy.String(64), unique=True)
    password: bytes = sqlalchemy.Column(sqlalchemy.LargeBinary(64))
    salt: bytes = sqlalchemy.Column(sqlalchemy.LargeBinary(64))
    last_login: datetime.datetime = sqlalchemy.Column(sqlalchemy.DateTime(True))
    twitch_id: int = sqlalchemy.Column(
        sqlalchemy.BigInteger, sqlalchemy.ForeignKey('users_twitch.tid'), unique=True)
    discord_id: int = sqlalchemy.Column(
        sqlalchemy.BigInteger, sqlalchemy.ForeignKey('users_discord.did'), unique=True)

    twitch_user = sqlalchemy.orm.relationship('TwitchUser', backref=sqlalchemy.orm.backref('user', uselist=False))
    discord_user = sqlalchemy.orm.relationship('DiscordUser', backref=sqlalchemy.orm.backref('user', uselist=False))

    def __repr__(self) -> str:
        return f"User(uid={self.uid}, username={self.username}, twitch_id={self.twitch_id}, discord_id={self.discord_id})"

    def set_password(self, password: str) -> None:
        self.salt = util.get_random_salt()
        self.password = util.hash_plaintext_password(password.encode('utf-8'), self.salt)

    def verify_password(self, password: str) -> bool:
        return self.password == util.hash_plaintext_password(password.encode('utf-8'), self.salt)


class UserLink(Base):
    __tablename__ = 'user_link'

    user_id = sqlalchemy.Column(sqlalchemy.BigInteger,
                                sqlalchemy.ForeignKey('users.uid',
                                                      ondelete="CASCADE",),
                                unique=True,
                                nullable=False)
    token = sqlalchemy.Column(sqlalchemy.CHAR(6),
                              nullable=False,
                              primary_key=True)
    valid_until = sqlalchemy.Column(sqlalchemy.DateTime(True),
                                    nullable=False)

    user = sqlalchemy.orm.relationship('User',
                                       backref=sqlalchemy.orm.backref("link", uselist=False))
