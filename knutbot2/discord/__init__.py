from __future__ import annotations
import enum
import discord
from typing import (
    List,
    TYPE_CHECKING,
    )
from knutbot2.command import CommandParameter, CommandOrigin
if TYPE_CHECKING:
    from knutbot2.discord.bot import DiscordBot
    from knutbot2.database import (
        User,
        ChatGroup,
        )


class DiscordCommandParameter(CommandParameter):

    def __init__(self,
                 chatgroup: ChatGroup,
                 user: User,
                 args: List[str],
                 message: discord.Message,
                 bot: DiscordBot,
                 ):
        super().__init__(chatgroup, user, CommandOrigin.DISCORD, args)
        self.message = message
        self.bot = bot


class DiscordReactionEventType(enum.Enum):
    ADD = enum.auto()
    REMOVE = enum.auto()


class DiscordReactionParameter(object):

    def __init__(self,
                 eventtype: DiscordReactionEventType,
                 event: discord.RawReactionActionEvent,
                 bot: DiscordBot,):
        self.event = event
        self.eventtype = eventtype
        self.bot = bot
