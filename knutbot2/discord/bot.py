from __future__ import annotations
import threading
import re
import queue
import asyncio

import logging
import time
from typing import (
    Optional,
    Any,
    )

import discord

from knutbot2 import (
    util,
    translator,
    )
from knutbot2.discord import (
    DiscordCommandParameter,
    DiscordReactionParameter,
    DiscordReactionEventType,
    )
from knutbot2.config import config
from knutbot2.command import (
    CommandParameter,
    CommandType,
    CommandOrigin,
    )
from knutbot2.database import session
from knutbot2.database.discorduser import DiscordUser
from knutbot2.database.chatgroup import ChatGroup
from knutbot2.database.user import User


class DiscordBot(discord.Client):

    _instance: Optional[DiscordBot] = None
    _thread: Optional[threading.Thread] = None

    @staticmethod
    @util.synchronized
    def init() -> None:
        if DiscordBot._instance is not None:
            raise Exception("Translator was initialized already")
        intents = discord.Intents.default()
        intents.members = True
        loop = asyncio.new_event_loop()
        db = DiscordBot(config['discord']['token'], 
                        intents=intents,
                        loop=loop)
        DiscordBot._instance = db
        loop.create_task(db.start(db.token))
        if not loop.is_running():
            DiscordBot._thread = threading.Thread(target=loop.run_forever(),
                                                  name="Discord Bot",
                                                  daemon=True)
            DiscordBot._thread.start()

    def __init__(self, token: str, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)
        self.token = token
        self.answer_queue: queue.Queue = queue.Queue()
        self.answer_thread = threading.Thread(target=asyncio.run,
                                              args=(self.run_answer(),),
                                              name="Discord Bot Answer",
                                              daemon=True)
        self.answer_thread.start()

    async def send_text(self, channel: discord.Channel, text: str) -> Any:
        return asyncio.run_coroutine_threadsafe(channel.send(text), self.loop).result()

    async def run_answer(self) -> None:
        while True:
            params, resp = self.answer_queue.get()
            channel = params.channel
            for text in resp.text:
                if text:
                    await self.send_text(channel, text)

    def get_user(self, message: discord.Message) -> User:
        discord_user = message.author
        du: DiscordUser = session().query(DiscordUser).filter(DiscordUser.did == discord_user.id).first()
        if not du:
            du = DiscordUser()
            du.did = discord_user.id
            u = User(discord_user=du)
            session().add(u)
        du.username = discord_user.name
        du.discriminator = discord_user.discriminator
        session().add(du)
        session().commit()
        return du.user  # type: ignore

    async def on_ready(self) -> None:
        logging.info('Logged in onto Discord as {0.user}'.format(self))

    async def on_message(self, message: discord.Message) -> None:
        if message.author == self.user:
            return
        if not message.guild:
            return
        user = self.get_user(message)
        cg = session().query(ChatGroup).filter(ChatGroup.discordid == message.guild.id).first()
        if not cg:
            if message.guild.owner != message.author:
                # no chatgroup initialized and it was not the owner
                return
            cg = session().query(ChatGroup).filter(ChatGroup.cg_id == -user.uid).first() # type: ignore
            if cg is None:
                cg = ChatGroup.create_empty(user, discordid=message.guild.id)  # type: ignore
        try:
            if message.content.startswith(cg.discordprefix):
                # remove multiple spaces by split+join, then split
                msgpart = ' '.join(message.content.split()).split()
                try:
                    cmdtype, cmdf = cg.get_command(CommandType.DISCORD, msgpart[0][len(cg.discordprefix):])
                except TypeError:
                    return
                if cmdtype == CommandType.COMMON:
                    translator.translate_from_discord(
                        translator.DiscordTranslatorParameter(
                            cmdf,
                            self.answer_queue,
                            message.channel,
                            CommandParameter(cg, user, CommandOrigin.DISCORD, msgpart[1:])
                            )
                        )
                elif cmdtype == CommandType.DISCORD:
                    param = DiscordCommandParameter(cg, user, msgpart[1:], message, self)
                    response = cmdf(param)
                    for text in response.text:
                        if text:
                            await message.channel.send(text)
        except KeyError:
            return
        finally:
            session().close()


    async def on_raw_reaction_add(self, event: discord.RawReactionActionEvent) -> None:
        if event.user_id == self.user.id:
            return
        cg = session().query(ChatGroup).filter(ChatGroup.discordid == event.guild_id).first()
        for reaction_cb in cg.discord_reaction_callbacks:
            if await reaction_cb(DiscordReactionParameter(DiscordReactionEventType.ADD, event, self)):
                return

    async def on_raw_reaction_remove(self, event: discord.RawReactionActionEvent) -> None:
        if event.user_id == self.user.id:
            return
        cg = session().query(ChatGroup).filter(ChatGroup.discordid == event.guild_id).first()
        for reaction_cb in cg.discord_reaction_callbacks:
            if await reaction_cb(DiscordReactionParameter(DiscordReactionEventType.REMOVE, event, self)):
                return

    async def on_guild_join(self, guild: int) -> None:
        pass

    async def on_guild_remove(self, guild: int) -> None:
        pass
