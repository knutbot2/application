from __future__ import annotations
from typing import (
    Dict,
    Any,
    Callable,
    Tuple,
    Optional,
    TYPE_CHECKING
    )
import json
import re
import asyncio
import threading

import discord

from knutbot2.permission import UserPermission
from knutbot2.command import Command, CommandResponse, CommandType
from knutbot2.database import (
    session,
    CG_COMMANDS_MEMBER_TYPE,
    ChatGroupModule,
    ChatGroup,
    )
from knutbot2.discord import (
    DiscordCommandParameter,
    DiscordReactionParameter,
    DiscordReactionEventType,
    )
if TYPE_CHECKING:
    from knutbot2.discord.bot import DiscordBot


class ReactionRole(object):

    EMOJI_RE = re.compile(r"<:\w+:[0-9]+>")
    MESSAGE_START = "React to this message with the following emoji to get the specified role"

    def __init__(self, cg_id: int, config: Dict[str, Any] = {'roles': {}}):
        self.cg_id = cg_id
        self.config = config
        self.subcmds = {
            'channel': self._reactionrole_channel,
            'addrole': self._reactionrole_add_role,
            'delrole': self._reactionrole_remove_role,
            'start': self._reactionrole_start,
            }

    def _commands(self) -> CG_COMMANDS_MEMBER_TYPE:
        return {
            CommandType.DISCORD: {'reactionrole': self.reactionrole, },
            }

    def save_config(self) -> None:
        cgm = ReactionRole.get_chatgroupmodule(self.cg_id)
        cgm.config = json.dumps(self.config)  # type: ignore # if cgm is none here, then WTF
        session().commit()

    @staticmethod
    def get_chatgroupmodule(cg_id: int) -> Optional[ChatGroupModule]:
        cgm: Optional[ChatGroupModule] = session().query(ChatGroupModule).filter(ChatGroupModule.cg_id == cg_id, ChatGroupModule.name == f"{'.'.join(__name__.split('.')[-2:])}").first()
        return cgm

    @Command(UserPermission.EDITOR)
    def reactionrole(self, param: DiscordCommandParameter) -> CommandResponse:
        try:
            subcmd = param.args[0]
        except IndexError:
            return CommandResponse(text=f"Options: {' '.join([k for k in self.subcmds])}")
        return self.subcmds[subcmd](param)

    async def _send_start_message(self,
                                  bot: DiscordBot,
                                  channel: discord.TextChannel) -> None:
        msg = await bot.send_text(channel, ReactionRole.MESSAGE_START)
        self.config['message-id'] = msg.id
        self.save_config()
        await self._update_start_message(bot)

    async def _update_start_message(self,
                                    bot: DiscordBot) -> None:
        print(self.config)
        if ('channel-id' not in self.config or
            'message-id' not in self.config):
            return
        channel = bot.get_channel(int(self.config['channel-id']))
        msg = asyncio.run_coroutine_threadsafe(channel.fetch_message(self.config['message-id']), bot.loop).result()
        messages = [ReactionRole.MESSAGE_START]
        emojis = []
        for emojiid, roleid in self.config['roles'].items():
            emoji = bot.get_emoji(int(emojiid))
            emojis.append(emoji)
            messages.append(f"{emoji}: {msg.guild.get_role(int(roleid)).mention}")
        message = '\n'.join(messages)
        asyncio.run_coroutine_threadsafe(msg.edit(content=message), bot.loop).result()
        for emoji in emojis:
            asyncio.run_coroutine_threadsafe(msg.add_reaction(emoji), bot.loop).result()

    async def _delete_start_message(self, bot: DiscordBot) -> None:
        channel = bot.get_channel(int(self.config['channel-id']))
        msg = asyncio.run_coroutine_threadsafe(channel.fetch_message(self.config['message-id']), bot.loop).result()
        asyncio.run_coroutine_threadsafe(msg.delete(), bot.loop).result()

    def _reactionrole_start(self, param: DiscordCommandParameter) -> CommandResponse:
        if not self.config['roles']:
            return CommandResponse(text="No roles set up")
        if 'channel-id' not in self.config:
            return CommandResponse(text="Channel not set")
        channel = param.bot.get_channel(int(self.config['channel-id']))
        threading.Thread(target=asyncio.run, args=(self._send_start_message(param.bot, channel),)).start()
        return CommandResponse("Done")

    def _reactionrole_add_role(self, param: DiscordCommandParameter) -> CommandResponse:
        if len(param.message.raw_role_mentions) != 1:
            return CommandResponse("When adding a new role exactly one role must be mentioned.", error=True)
        matches = ReactionRole.EMOJI_RE.findall(param.message.content)
        if len(matches) != 1:
            return CommandResponse("When adding a new role exactly one **custom** emoji must be mentioned.", error=True)
        emoji_id = matches[0].split(':')[2][:-1]
        if param.bot.get_emoji(int(emoji_id)) is None:
            return CommandResponse("I don't have access to that emoji", error=True)
        if 'roles' not in self.config:
            self.config['roles'] = {}
        self.config['roles'][emoji_id] = param.message.raw_role_mentions[0]
        self.save_config()
        threading.Thread(target=asyncio.run, args=(self._update_start_message(param.bot),)).start()
        return CommandResponse("Added role to reactions")

    def _reactionrole_remove_role(self, param: DiscordCommandParameter) -> CommandResponse:
        if len(param.message.raw_role_mentions) != 1:
            return CommandResponse("When removing a role exactly one role must be mentioned.")
        if 'roles' not in self.config:
            self.config['roles'] = {}
        found = False
        for emoji_id, role_id in self.config['roles'].items():
            if role_id == param.message.raw_role_mentions[0]:
                del self.config['roles'][emoji_id]
                found = True
                break
        if found:
            self.save_config()
            threading.Thread(target=asyncio.run, args=(self._update_start_message(param.bot),)).start()
            return CommandResponse("Removed role to reactions")
        else:
            return CommandResponse(f"Role was not registered with {__name__.split('.')[-1]}")

    def _reactionrole_channel(self, param: DiscordCommandParameter) -> CommandResponse:
        if len(param.message.channel_mentions) != 1:
            return CommandResponse("When setting the channel exactly one channel must be mentioned.")
        channel = param.message.channel_mentions[0]
        if not isinstance(channel, discord.TextChannel):
            return CommandResponse("The channel must be a text channel.")
        if 'channel-id' in self.config and 'message-id' in self.config:
            threading.Thread(target=asyncio.run, args=(self._delete_start_message(param.bot),)).start()
        self.config['channel-id'] = param.message.channel_mentions[0].id
        self.save_config()
        return CommandResponse("Channel set.")

    async def react(self, param: DiscordReactionParameter) -> bool:
        if ('message-id' not in self.config or
                self.config['message-id'] != param.event.message_id):
            return False
        print(param.event.emoji, str(param.event.emoji), str(param.event.emoji).encode('utf-8'), str(param.event.emoji).encode('ascii', 'namereplace'))
        emoji_id = str(param.event.emoji.id)
        if emoji_id in self.config['roles']:
            guild = param.bot.get_guild(param.event.guild_id)
            role = guild.get_role(self.config['roles'][emoji_id])
            member = guild.get_member(param.event.user_id)
            if member:
                if param.eventtype == DiscordReactionEventType.ADD:
                    await member.add_roles(role, reason="Reaction to reactionroles")
                else:
                    await member.remove_roles(role, reason="Reaction to reactionroles")
            return True
        return False


def enable(chatgroup: ChatGroup) -> None:
    session().add(chatgroup)
    cgm = ReactionRole.get_chatgroupmodule(chatgroup.cg_id)
    rr = ReactionRole(chatgroup.cg_id, json.loads(cgm.config) if cgm and cgm.config else {})
    chatgroup.add_commands(rr._commands())
    chatgroup.discord_reaction_callbacks.append(rr.react)


def disable(chatgroup: ChatGroup) -> None:
    cmds = chatgroup.commands
    rr = ReactionRole(chatgroup.cg_id)
    for key in rr._commands():
        del cmds[key]
