from typing import Optional
import re

import knutbot2.database as db


user_mention_re = re.compile(r"<@!?([0-9]+)>")


def get_user_from_mention_string(string: str) -> Optional[db.User]:
    match = user_mention_re.fullmatch(string)
    if match:
        did = match[1]
        session = db.session()
        du = session.query(db.DiscordUser).filter(db.DiscordUser.did == did).first()
        if du:
            return du.user  # type: ignore
    return None
