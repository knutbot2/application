from __future__ import annotations
from typing import (
    Optional,
    )
import threading
import queue
import logging
# import time for debug right now
import time
import random

from knutbot2 import util
from knutbot2.command import (
    CommonCommandWork,
    CommandResponse,
    )


async_queue: queue.Queue = queue.Queue()
sync_queue: queue.Queue = queue.Queue()


class Evaluator(object):

    def __init__(self) -> None:
        self.async_workers = []
        for i in range(5):
            t = threading.Thread(name=f"Async Evaluator Worker {i}",
                                 target=self.run,
                                 args=(async_queue,),
                                 daemon=True)
            t.start()
            self.async_workers.append(t)
        self.sync_worker = threading.Thread(name="Sync Evaluator Worker",
                                            target=self.run,
                                            args=(sync_queue,),
                                            daemon=True)
        self.sync_worker.start()

    def run(self, workqueue: queue.Queue) -> None:
        from knutbot2.database import session
        while True:
            try:
                workitem: CommonCommandWork = workqueue.get()
                a = workitem.fn(workitem.params)
                a.trans_cache_id = workitem.trans_cache_id
                workitem.answer_queue.put(a)
            except Exception:
                # Catch all exceptions here so the worker does not die
                logging.exception(f"A completely unexpected exception occured evaluating <{workitem}>, dropping that input! ({threading.current_thread().name})")
                workitem.answer_queue.put(
                    CommandResponse(
                        '',
                        fatal_error=True,
                        trans_cache_id=workitem.trans_cache_id))
            finally:
                workqueue.task_done()
                session().close()


evaluator: Optional[Evaluator] = None


@util.synchronized
def init() -> None:
    global evaluator
    if evaluator is not None:
        raise Exception("Evaluator was initialized already")
    evaluator = Evaluator()
