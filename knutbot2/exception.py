class NotInitializedError(Exception):
    pass


class CommandNameAlreadyExists(Exception):
    pass


class CooldownHasNoSecondsError(Exception):
    pass
