from __future__ import annotations
import enum
import traceback
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from knutbot2.database import (
        ChatGroup,
        User,
        )


class UserPermission(enum.IntFlag):
    EVERYONE = 0
    REGULAR = 1
    VIP = 2 | REGULAR
    SUBSCRIBER = 4 | REGULAR
    MODERATOR = 64 | SUBSCRIBER | VIP | REGULAR | EVERYONE
    EDITOR = 256 | MODERATOR
    OWNER = 2048-1


def get_user_permission(user: User, chatgroup: ChatGroup) -> UserPermission:
    if user == chatgroup.owner:
        return UserPermission.OWNER
    from knutbot2.database import (
        UserPermission as DBUserPermission,
        User,
        session,
    )
    result = session().query(DBUserPermission).filter(DBUserPermission.user_id == user.uid).filter(DBUserPermission.cg_id == chatgroup.cg_id).first()
    return UserPermission(result.userpermission) if result else UserPermission.EVERYONE


def get_cmd_required_permission(cmdname: str,
                                chatgroup: ChatGroup,
                                default: UserPermission) -> UserPermission:
    from knutbot2.database import (
        CommandPermission,
        session,
    )
    result = session().query(CommandPermission).filter(CommandPermission.command_name == cmdname).filter(CommandPermission.cg_id == chatgroup.cg_id).first()
    return UserPermission(result.required_permission) if result else default
