import threading
import random
import queue
import logging
from typing import (
    Dict,
    Any,
    Callable,
    Optional,
    )

import twitchio.dataclasses
import discord

from knutbot2 import (
    evaluator,
    util,
    exception,
    )
from knutbot2.command import CommandParameter


#####################
# PARAMETER CLASSES #
#####################
class TranslatorParameter(object):

    def __init__(self,
                 fn: Callable,
                 answer_queue: queue.Queue,
                 fn_params: CommandParameter):
        self.fn = fn
        self.answer_queue = answer_queue
        self.fn_params = fn_params


class DiscordTranslatorParameter(TranslatorParameter):

    def __init__(self,
                 fn: Callable,
                 answer_queue: queue.Queue,
                 channel: discord.TextChannel,
                 fn_params: CommandParameter):
        super().__init__(fn, answer_queue, fn_params)
        self.channel = channel


class TwitchTranslatorParameter(TranslatorParameter):

    def __init__(self,
                 fn: Callable,
                 answer_queue: queue.Queue,
                 channel: twitchio.dataclasses.Channel,
                 fn_params: CommandParameter):
        super().__init__(fn, answer_queue, fn_params)
        self.channel = channel


class Translator(object):

    def __init__(self) -> None:
        self.response_queue: queue.Queue = queue.Queue()
        self.cache: Dict[int, Any] = {}
        self.worker = threading.Thread(target=self.run,
                                       name="Translator Response Thread",
                                       daemon=True)
        self.worker.start()

    def run(self) -> None:
        while True:
            try:
                resp = self.response_queue.get()
                params = self.cache[resp.trans_cache_id]
                del self.cache[resp.trans_cache_id]
                # replace stuff back
                params.answer_queue.put((params, resp))
            except Exception:
                # Catch all exceptions here so the worker does not die
                logging.exception(f"A completely unexpected exception occured back-translating <{resp}>, dropping that input! ({threading.current_thread().name})")
            finally:
                self.response_queue.task_done()

    def translate_from_discord(self, params: DiscordTranslatorParameter) -> None:
        i = self._get_random_cache_id()
        self.cache[i] = params
        params.fn(self.response_queue, i, params.fn_params)

    def translate_from_twitch(self, params: TwitchTranslatorParameter) -> None:
        i = self._get_random_cache_id()
        self.cache[i] = params
        params.fn(self.response_queue, i, params.fn_params)

    @util.synchronized
    def _get_random_cache_id(self) -> int:
        while True:
            i = random.randint(0, 10000)
            if i not in self.cache:
                return i


translator: Optional[Translator] = None


# convenience functions to not have to call translator.translator.*

def translate_from_discord(params: DiscordTranslatorParameter) -> None:
    if translator is None:
        raise exception.NotInitializedError()
    translator.translate_from_discord(params)


def translate_from_twitch(params: TwitchTranslatorParameter) -> None:
    if translator is None:
        raise exception.NotInitializedError()
    translator.translate_from_twitch(params)


@util.synchronized
def init() -> None:
    global translator
    if translator is not None:
        raise Exception("Translator was initialized already")
    translator = Translator()


