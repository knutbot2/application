from __future__ import annotations
import enum
from typing import (
    List,
    TYPE_CHECKING,
    )
from knutbot2.command import CommandParameter, CommandOrigin
if TYPE_CHECKING:
    from twitchio.dataclasses import Message
    from knutbot2.twitch.bot import TwitchBot
    from knutbot2.database import (
        User,
        ChatGroup,
        )


class TwitchCommandParameter(CommandParameter):

    def __init__(self,
                 chatgroup: ChatGroup,
                 user: User,
                 args: List[str],
                 message: Message,
                 bot: TwitchBot,
                 ):
        super().__init__(chatgroup, user, CommandOrigin.TWITCH, args)
        self.message = message
        self.bot = bot
