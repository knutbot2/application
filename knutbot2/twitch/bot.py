from __future__ import annotations
import threading
import queue
import asyncio
from typing import (
    Optional,
    )

from twitchio.ext.commands import Bot
from twitchio import dataclasses

from knutbot2 import (
    util,
    translator,
    )
from knutbot2.twitch import TwitchCommandParameter
from knutbot2.config import config
from knutbot2.command import (
    CommandParameter,
    CommandType,
    CommandOrigin,
    )
from knutbot2.database import session
from knutbot2.database.twitchuser import TwitchUser
from knutbot2.database.twitchchannel import TwitchChannel
from knutbot2.database.chatgroup import ChatGroup
from knutbot2.database.user import User


class TwitchBot(Bot):

    _instance: Optional[TwitchBot] = None
    _thread: Optional[threading.Thread] = None

    @staticmethod
    @util.synchronized
    def init() -> None:
        if TwitchBot._instance is not None:
            raise Exception("TwitchBot was initialized already")
        tb = TwitchBot(config['twitch']['username'],
                       config['twitch']['token'],
                       config['twitch']['client_id'],
                       config['twitch']['client_secret'],)
        TwitchBot._instance = tb
        TwitchBot._thread = threading.Thread(target=tb.run,
                                             name="Twitch Bot",
                                             daemon=True)
        TwitchBot._thread.start()

    def __init__(self, nick: str, oauth: str, client_id: str, client_secret: str) -> None:
        super().__init__(
            irc_token=f'oauth:{oauth}',
            client_id=client_id,
            client_secret=client_secret,
            prefix='',
            nick=nick.lower(),
            loop=asyncio.new_event_loop())
        self.answer_queue: queue.Queue = queue.Queue()
        self.answer_thread = threading.Thread(target=asyncio.run,
                                              args=(self.run_answer(),),
                                              name="Twitch Bot Answer",
                                              daemon=True)
        self.answer_thread.start()

    async def run_answer(self) -> None:
        while True:
            params, resp = self.answer_queue.get()
            channel = params.channel
            for text in resp.text:
                if text:
                    asyncio.run_coroutine_threadsafe(channel.send(text), self.loop).result()

    async def event_ready(self) -> None:
        channels = [self.nick]
        ids = []
        for tc in session().query(TwitchChannel).all():
            ids.append(tc.twitchid)
        if ids:
            users = await self.get_users(*ids)
            for user in users:
                channels.append(user.login)
        await self.join_channels(channels)

    def get_user(self, message: dataclasses.Message) -> Optional[User]:
        author = message.author
        if author.id != 0:
            tu = session().query(TwitchUser).filter(TwitchUser.tid == author.id).first()
            display = message.tags['display-name'] if 'display-name' in message.tags else author.name
            if not tu:
                tu = TwitchUser(tid=author.id,
                                username=author.name,
                                displayname=display)
                session().add(tu)
                u = User()
                u.twitch_user = tu
                session().add(u)
                session().commit()
            else:
                tu.username = author.name
                tu.displayname = display
                session().commit()
        else:
            tu = session().query(TwitchUser).filter(TwitchUser.username == author.name).first()
            if not tu:
                return None
        return tu.user  # type: ignore

    async def event_message(self, message: dataclasses.Message) -> None:
        author = message.author
        if author.name == self.nick:
            return

        user = self.get_user(message)
        if user is None:
            return
        channel = message.channel
        if channel.name == self.nick:
            if message.content.startswith("!joinme"):
                tc = session().query(TwitchChannel).filter(TwitchChannel.twitchid == author.id).first()
                if tc is None:
                    session().add(TwitchChannel(twitchid=author.id))
                    session().commit()
                await self.join_channels([author.name])
                await channel.send(f"@{user.twitch_user.displayname} I joined your channel!")
                return
            else:
                message_id = message.tags['id']
                await channel.send(f"/delete {message_id}")
                await channel.send("If you want me to join your channel, type !joinme")
        channelowner = session().query(TwitchUser).filter(TwitchUser.username == channel.name).first()
        if channelowner is None:
            # Never even saw the channel owner, ignore it
            return
        cg = session().query(ChatGroup).filter(ChatGroup.twitchid == channelowner.tid).first()
        if cg is None:
            # a chat group was not created yet, only respond to owner
            if channel.name != author.name:
                return
            cg = session().query(ChatGroup).filter(ChatGroup.cg_id == -user.uid).first()  # type: ignore
            if cg is None:
                cg = ChatGroup.create_empty(user, twitchid=channelowner.tid)  # type: ignore
        try:
            if message.content.startswith(cg.twitchprefix):
                # remove multiple spaces by split+join, then split
                msgpart = ' '.join(message.content.split()).split()
                try:
                    cmdtype, cmdf = cg.get_command(CommandType.TWITCH, msgpart[0][len(cg.twitchprefix):])
                except TypeError:
                    return
                if cmdtype == CommandType.COMMON:
                    translator.translate_from_twitch(
                        translator.TwitchTranslatorParameter(
                            cmdf,
                            self.answer_queue,
                            channel,
                            CommandParameter(cg, user, CommandOrigin.TWITCH, msgpart[1:])
                            )
                        )
                elif cmdtype == CommandType.TWITCH:
                    param = TwitchCommandParameter(cg, user, msgpart[1:], message, self)
                    response = await cmdf(param)
                    for text in response.text:
                        if text:
                            await message.channel.send(text)
        except KeyError:
            return
        finally:
            session().close()

