from typing import (
    Callable,
    Any,
    )
import threading
import os
import hashlib
import time


# Synchronized decorator
def synchronized(func: Callable) -> Callable:
    lock = threading.Lock()

    def sync_func(*args: Any, **kwargs: Any) -> Any:
        with lock:
            return func(*args, **kwargs)
    return sync_func


def get_random_salt() -> bytes:
    return os.urandom(64)


def hash_plaintext_password(plaintext: bytes, salt: bytes) -> bytes:
    return hashlib.pbkdf2_hmac('sha256', plaintext, salt, 500000)
